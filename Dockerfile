FROM node:14-alpine as build
ARG git_pat
ARG package_scope=@emritio
RUN echo "${package_scope}:registry=https://npm.pkg.github.com/" > .npmrc && \
    echo "//npm.pkg.github.com/:_authToken=${git_pat}" >> .npmrc
COPY ./package*.json ./
RUN npm ci
COPY . .
RUN npm run build


FROM node:14-alpine
RUN apk update && apk add curl
WORKDIR /usr/src/app
COPY newrelic.js package*.json ./
COPY --from=build ./.npmrc ./
RUN npm ci --production && rm .npmrc
COPY --from=build ./dist ./dist

# TODO
EXPOSE ENTER PORT NUMBER HERE

CMD npm run start:prod
