import { injectable, inject } from 'inversify';
import { SampleEvent } from '../../domain/events/sample.event';
import { Logger } from '../../shared/services/logger/logger';
import { Saga } from '../../shared/events/saga-explorer';

@injectable()
export class SampleSaga {
  constructor(@inject(Logger) private readonly logger: Logger) {}

  @Saga()
  sampleSaga(event: SampleEvent): undefined {
    if (event instanceof SampleEvent) {
      this.logger.log('I could return a command here');
    }

    return undefined;
  }
}
