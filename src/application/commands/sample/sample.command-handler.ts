import { injectable, inject } from 'inversify';
import { SampleEntity } from '../../../domain/entities/sample.entity';
import { SampleRepository } from '../../../domain/repositories/sample.repository';
import { inspect } from 'util';
import { EventPublisher } from '../../../shared/events/event-publisher';
import { Command } from '../../../shared/events/command';
import { RegisterCommandHandler } from '../../../shared/events/command-handler-explorer';
import { CommandHandler } from '../../../shared/events/command.handler';
import { Logger } from '../../../shared/services/logger/logger';

export class SampleCommand extends Command<SampleCommand> {}

@RegisterCommandHandler(SampleCommand)
@injectable()
export class SampleCommandHandler implements CommandHandler<SampleCommand> {
  constructor(
    @inject(Logger) private readonly logger: Logger,
    @inject(SampleRepository) private readonly sampleRepository: SampleRepository,
    @inject(EventPublisher) private readonly eventPublisher: EventPublisher
  ) {}

  async execute(_data: SampleCommand): Promise<void> {
    const sampleEntity = SampleEntity.create();

    try {
      this.logger.log('Im executed inside a command handler!');

      await this.sampleRepository.save(sampleEntity);
    } catch (e) {
      this.logger.error(`An error occurred \n ${inspect(e)}`);

      throw e;
    } finally {
      this.eventPublisher.mergeObjectContext(sampleEntity);

      await sampleEntity.commit();
    }
  }
}
