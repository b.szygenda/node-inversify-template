import { injectable, inject } from 'inversify';
import { QueryHandler } from '../../../shared/events/query-handler';
import { RegisterQueryHandler } from '../../../shared/events/query-handler-explorer';
import { Logger } from '../../../shared/services/logger/logger';
import { Query } from '../../../shared/events/query';

export class SampleQuery extends Query<SampleQuery> {}

export interface SampleQueryResult {
  isSample: boolean;
}

@RegisterQueryHandler(SampleQuery)
@injectable()
export class SampleQueryHandler implements QueryHandler<SampleQuery, SampleQueryResult> {
  constructor(@inject(Logger) private readonly logger: Logger) {}

  execute(_data: SampleQuery): SampleQueryResult {
    this.logger.log('Im executed inside a query handler!');

    return {
      isSample: true,
    };
  }
}
