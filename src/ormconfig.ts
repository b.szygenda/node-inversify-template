import { ConnectionOptions } from 'typeorm';
import { EntitySchemas } from './domain/entities/entity-schemas';

const config: ConnectionOptions = {
  type: 'postgres',
  host: process.env.POSTGRES_HOST,
  port: Number(process.env.POSTGRES_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  migrationsRun: process.env.RUN_MIGRATIONS === 'true',
  synchronize: process.env.DB_SYNC === 'true',
  entities: EntitySchemas,
  migrations: [`${__dirname}/../**/migrations/*.js`],
  cli: { migrationsDir: 'src/migrations' },
  migrationsTransactionMode: 'each',
};

export default config;
