import { SampleEntity } from '../entities/sample.entity';
import { BasicEvent } from '../../shared/events/basic.event';

export class SampleEvent implements BasicEvent {
  readonly entityName: string = SampleEntity.name;
  readonly userId: null;
  readonly sampleEntityId: string;
  readonly createdAt = new Date();

  static createFromSampleEntity(entity: SampleEntity): SampleEvent {
    return new SampleEvent({
      userId: null,
      sampleEntityId: entity.id,
    });
  }

  constructor(partial: Partial<SampleEvent>) {
    Object.assign(this, partial);
  }
}
