import { StatusCodes } from 'http-status-codes';
import { Errors } from '../../shared/errors';
import { IExceptionParameters, HttpException } from '../../shared/exceptions';

export class SampleException extends HttpException {
  static createSampleDoesNotExistException(): SampleException {
    return new SampleException({
      message: 'Sample does not exist',
      status: StatusCodes.NOT_FOUND,
      errorCode: Errors.entityNotFound,
    });
  }

  private constructor({ errorCode, message, status, details }: IExceptionParameters & { status: StatusCodes }) {
    super(
      HttpException.createBody(
        {
          errorCode,
          message,
          details,
        },
        status
      ),
      status
    );
  }
}
