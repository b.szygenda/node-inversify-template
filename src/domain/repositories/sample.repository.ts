import { injectable } from 'inversify';
import { SampleEntity } from '../entities/sample.entity';
import { BaseRepository } from '../../shared/repositories/base.repository';
import { ClassConstructor } from 'class-transformer';

@injectable()
export class SampleRepository extends BaseRepository<SampleEntity> {
  protected readonly entity: ClassConstructor<SampleEntity> = SampleEntity;

  constructor() {
    super();
  }

  findOneById(id: string): Promise<SampleEntity | undefined> {
    return this.repository.findOne(id);
  }

  async save(entity: SampleEntity): Promise<void> {
    await this.repository.save(entity);
  }
}
