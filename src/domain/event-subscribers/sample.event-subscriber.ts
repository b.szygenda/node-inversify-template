import { injectable, inject } from 'inversify';
import { SampleEvent } from '../events/sample.event';
import { inspect } from 'util';
import { Subscriber } from '../../shared/events/subscriber';
import { Logger } from '../../shared/services/logger/logger';

@injectable()
export class SampleEventSubscriber implements Subscriber<SampleEvent> {
  constructor(@inject(Logger) private readonly logger: Logger) {}

  async subscribe(publishedEvent: SampleEvent): Promise<void> {
    this.logger.log(`Sample event published with payload \n ${inspect(publishedEvent)}`);
  }
}
