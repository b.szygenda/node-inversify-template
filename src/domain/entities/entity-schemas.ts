import { EntitySchema } from 'typeorm';
import { SampleEntity } from './sample.entity';
import { BaseDatesColumnsEntitySchema } from '../../shared/repositories/base.entity';

export const SampleEntitySchema = new EntitySchema<SampleEntity>({
  name: SampleEntity.name,
  target: SampleEntity,
  tableName: 'sample_entity',
  columns: {
    id: {
      type: 'uuid',
      primary: true,
      generated: 'uuid',
      name: 'sample_entity_id',
    },
    ...BaseDatesColumnsEntitySchema,
  },
});

export const EntitySchemas: EntitySchema[] = [SampleEntitySchema];
