import { SampleEvent } from '../events/sample.event';
import { v4 } from 'uuid';
import { IBaseEntityId, IBaseEntityDates } from '../../shared/repositories/base.entity';
import { AggregateRoot } from '../../shared/events/aggregate-root';

export class SampleEntity extends AggregateRoot implements IBaseEntityId, IBaseEntityDates {
  id: string;
  createdDate: Date;
  deletedDate: Date | null;
  updatedDate: Date;

  static create(): SampleEntity {
    const entity = new SampleEntity();

    return entity.create();
  }

  constructor(partial?: Partial<SampleEntity>) {
    super();
    Object.assign(this, partial);
  }

  private create(): SampleEntity {
    this.id = v4();

    this.publish<SampleEvent>(SampleEvent.createFromSampleEntity(this));

    return this;
  }
}
