import { injectable, inject } from 'inversify';
import { Logger } from '../../shared/services/logger/logger';

@injectable()
export class SampleService {
  constructor(@inject(Logger) private readonly logger: Logger) {}

  sampleMethod(): void {
    this.logger.log('Sample method executed');
  }
}
