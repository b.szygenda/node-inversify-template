import { SampleService } from './sample.service';
import { Logger } from '../../shared/services/logger/logger';

describe('Sample service unit tests', () => {
  let service: SampleService;
  let loggerMock: Logger;

  beforeEach(() => {
    loggerMock = {
      log: jest.fn(),
    } as unknown as Logger;

    service = new SampleService(loggerMock);
  });

  beforeAll(() => {
    jest.resetAllMocks();
  });

  it('Should call a console.log command', () => {
    jest.spyOn(loggerMock, 'log');
    service.sampleMethod();

    expect(loggerMock.log).toHaveBeenCalledTimes(1);
  });
});
