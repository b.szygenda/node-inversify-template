import { Container as InversifyContainer } from 'inversify';
import { Config } from '../utils/config/config';
import { EventRepository } from '../utils/event-storage/event.repository';
import { SampleEventSubscriber } from '../domain/event-subscribers/sample.event-subscriber';
import { SampleRepository } from '../domain/repositories/sample.repository';
import { SampleService } from '../domain/services/sample.service';
import { SampleCommandHandler } from '../application/commands/sample/sample.command-handler';
import { SampleQueryHandler } from '../application/queries/sample/sample.query-handler';
import { SampleEvent } from '../domain/events/sample.event';
import { EventFactory } from '../utils/event-storage/event-factory';
import { EventPublisher } from '../shared/events/event-publisher';
import { Subscriber } from '../shared/events/subscriber';
import { QueryHandlerExplorer } from '../shared/events/query-handler-explorer';
import { QueryBus } from '../shared/events/query-bus';
import { EventStorageSubscriber } from '../shared/events/event-storage.subscriber';
import { SagaHandler } from '../shared/events/saga-handler';
import { CommandHandlerExplorer } from '../shared/events/command-handler-explorer';
import { CommandBus } from '../shared/events/command-bus';
import { SagaExplorer } from '../shared/events/saga-explorer';
import { Logger, DefaultLogger } from '../shared/services/logger/logger';

export class Container {
  private static instance: InversifyContainer;

  static getInstance(): InversifyContainer {
    if (this.instance) {
      return this.instance;
    }

    const container = new Container();

    container.init();
    container.initEventPublisher();
    container.initCommandBus();
    container.initQueryBus();
    container.initSagas();

    this.instance = container.inversifyContainer;

    return this.instance;
  }

  private readonly inversifyContainer: InversifyContainer;

  constructor() {
    this.inversifyContainer = new InversifyContainer();
  }

  private initEventPublisher(): void {
    this.inversifyContainer
      .bind(EventPublisher)
      .toDynamicValue((context) => {
        const { container } = context;
        const subscribers = new Map<string, Subscriber<any>[]>([[SampleEvent.name, [container.get(SampleEventSubscriber)]]]);

        const globalSubscribers: Subscriber<any>[] = [container.get(SagaHandler)];

        return new EventPublisher(subscribers, container.get(EventStorageSubscriber), container.get(Logger), globalSubscribers);
      })
      .inSingletonScope();
  }

  private initCommandBus(): void {
    this.inversifyContainer
      .bind(CommandHandlerExplorer)
      .toDynamicValue(({ container }) => CommandHandlerExplorer.create(container))
      .inSingletonScope();

    this.inversifyContainer
      .bind(CommandBus)
      .toDynamicValue(({ container }) => {
        const commandExplorer = container.get(CommandHandlerExplorer);

        return new CommandBus(commandExplorer.commandHandlers);
      })
      .inSingletonScope();
  }

  private initSagas(): void {
    this.inversifyContainer.bind(SagaExplorer).toDynamicValue(({ container }) => SagaExplorer.create(container));

    this.inversifyContainer
      .bind(SagaHandler)
      .toDynamicValue(({ container }) => {
        const { sagas } = container.get(SagaExplorer);

        return new SagaHandler(sagas, container, container.get(Logger));
      })
      .inSingletonScope();
  }

  private initQueryBus() {
    this.inversifyContainer
      .bind(QueryHandlerExplorer)
      .toDynamicValue(({ container }) => QueryHandlerExplorer.create(container))
      .inSingletonScope();

    this.inversifyContainer.bind(QueryBus).toDynamicValue(({ container }) => {
      const queryHandlerExplorer = container.get(QueryHandlerExplorer);

      return new QueryBus(queryHandlerExplorer.queryHandlers);
    });
  }

  private init(): void {
    this.inversifyContainer
      .bind(Config)
      .toDynamicValue(() => Config.load())
      .inSingletonScope();

    this.inversifyContainer.bind(EventRepository).to(EventRepository).inSingletonScope();

    this.inversifyContainer.bind(EventFactory).to(EventFactory).inSingletonScope();

    this.inversifyContainer.bind(Logger).to(DefaultLogger).inSingletonScope();

    /**
     * TODO: Remove samples below
     */

    this.inversifyContainer.bind(SampleEventSubscriber).to(SampleEventSubscriber).inSingletonScope();

    this.inversifyContainer.bind(SampleRepository).to(SampleRepository).inSingletonScope();

    this.inversifyContainer.bind(SampleService).to(SampleService).inSingletonScope();

    this.inversifyContainer.bind(SampleCommandHandler).to(SampleCommandHandler).inSingletonScope();

    this.inversifyContainer.bind(SampleQueryHandler).to(SampleQueryHandler).inSingletonScope();

    /*
      TODO: Add missing injectables
     */
  }
}
