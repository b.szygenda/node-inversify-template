/* eslint-disable max-len */
import { MigrationInterface, QueryRunner } from 'typeorm';

export class SampleMigration1649230258931 implements MigrationInterface {
  name = 'SampleMigration1649230258931';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "sample_entity" ("sample_entity_id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created_date" TIMESTAMP NOT NULL DEFAULT now(), "updated_date" TIMESTAMP NOT NULL DEFAULT now(), "deleted_date" TIMESTAMP, CONSTRAINT "PK_86d4a13ffda3ee7716b023d0b82" PRIMARY KEY ("sample_entity_id"))`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "sample_entity"`);
  }
}
