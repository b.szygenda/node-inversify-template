import { Event } from './event.entity';
import { BaseEventRepository } from '../../shared/events/event.repository';
import { ClassConstructor } from 'class-transformer';
import { Event as EventInterface } from '../../shared/events/event.entity';

export class EventRepository extends BaseEventRepository {
  protected readonly entity: ClassConstructor<EventInterface> = Event;

  constructor() {
    super();
  }
}
