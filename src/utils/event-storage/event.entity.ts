import { Column, CreateDateColumn, Entity, Generated, PrimaryGeneratedColumn } from 'typeorm';
import { BasicEvent } from '../../shared/events/basic.event';
import { ClassConstructor } from 'class-transformer';
import { FromNumberValueTransformer } from '../../shared/typeorm/from-number-value.transformer';
import { Event as EventInterface } from '../../shared/events/event.entity';

@Entity()
export class Event<TEventData = Record<string, any>> implements EventInterface {
  static create(event: ClassConstructor<any> & BasicEvent) {
    return new Event({
      data: event,
      type: event.constructor.name,
      entity: event.entityName,
    });
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Generated('increment')
  @Column({
    type: 'bigint',
    transformer: new FromNumberValueTransformer(),
  })
  sequence: number;

  @Column({ type: 'jsonb' })
  data: TEventData;

  @Column()
  type: string;

  @Column()
  entity: string;

  @CreateDateColumn()
  createdAt: Date;

  constructor(partial: Partial<Event>) {
    Object.assign(this, partial);
  }
}
