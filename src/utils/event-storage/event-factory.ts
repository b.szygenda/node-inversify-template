import { injectable } from 'inversify';
import { Event } from './event.entity';
import { BaseEventFactory } from '../../shared/events/base-event-factory';
import { ClassConstructor } from 'class-transformer';

@injectable()
export class EventFactory extends BaseEventFactory {
  protected readonly eventEntity: ClassConstructor<Event> = Event;
}
