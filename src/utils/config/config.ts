import { plainToClass, Type } from 'class-transformer';
import { IsString, validate, ValidateNested } from 'class-validator';
import { injectable } from 'inversify';
import { inspect } from 'util';
import { ClassValidatorExceptionMapper } from '../../shared/class-validator.exception-mapper';

class ServicesConfig {
  @IsString()
  readonly apiKey: string;
}

@injectable()
export class Config {
  static load(): Config {
    const plainConfig = {
      services: {
        apiKey: process.env.INTERNAL_API_KEY as string,
      },
    } as Partial<Config>;

    return plainToClass(Config, plainConfig);
  }

  @Type(() => ServicesConfig)
  @ValidateNested()
  readonly services!: ServicesConfig;

  async validate(): Promise<void> {
    const errors = await validate(this);

    if (errors.length) {
      const mapper = new ClassValidatorExceptionMapper();

      throw new Error(`Please check yours .env and Config class. Missing or wrong values: ${inspect(mapper.map(errors))}`);
    }
  }
}
