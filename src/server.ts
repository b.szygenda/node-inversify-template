import 'reflect-metadata';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express, { Application, Request, Response } from 'express';
import 'express-async-errors';
import helmet from 'helmet';
import morgan from 'morgan';
import BaseRouterV2 from './ui/rest';
import { databaseTransactionMiddleware } from './shared/repositories/database-transaction.middleware';
import { xssParser } from './shared/sanitizer/xss.parser';
import { Sanitizer } from './shared/sanitizer/sanitizer';
import { trimmer } from './shared/sanitizer/trimmer';

const app: Application = express();

/** **********************************************************************************
 *                              Initialize Basic Settings
 ********************************************************************************** */

app.use(
  cors({
    origin: /(https?):\/\/([\S]+)\.(domain.com)/, // TODO: Replace domain.com with the domain of the application
    credentials: true,
  })
);

app.use(
  express.json({
    limit: '3mb',
    // We need raw body to calculate sha256 of shopify webhooks
    verify: (req: Request, res: Response, buf: Buffer) => {
      Object.assign(req, {
        rawBody: buf.toString('utf-8'),
      });
    },
  })
);
app.use(cookieParser());
app.use(helmet());
app.use(Sanitizer.initializeMiddleware([xssParser, trimmer]));

/** **********************************************************************************
 *                              Initialize Logger
 ********************************************************************************** */

app.use(
  morgan(
    ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" ' +
      ':status :res[content-length] ":referrer" ":user-agent" :req[Authorisation]'
  )
);

/** **********************************************************************************
 *                              Initialize Api Configuration
 ********************************************************************************** */
app.use(databaseTransactionMiddleware);

// TODO: Remember to add service name
app.use('/enter-service-name-here/api/v2', BaseRouterV2);

/** **********************************************************************************
 *                              Initialize Error Handling Middleware
 ********************************************************************************** */

// app.use((err: Error, _req: Request, res: Response, _next: NextFunction) => {
// TODO: Initialize exception error handler (TODO)
// });

/** **********************************************************************************
 *                              Export Server
 ********************************************************************************** */

export default app;
