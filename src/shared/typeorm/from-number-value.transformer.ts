import { ValueTransformer } from 'typeorm';

export class FromNumberValueTransformer implements ValueTransformer {
  from(value: any): any {
    return Number(value);
  }

  to(value: any): any {
    return value;
  }
}
