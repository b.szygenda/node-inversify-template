// @ts-ignore
import * as als from 'async-local-storage';

als.enable();
als.enableLinkedTop();

export class AsyncLocalStorage {
  static set<TData>(key: string, value: TData): void {
    als.scope();
    als.set(key, value, true);
  }

  static get<TData>(key: string): TData | undefined {
    return als.get(key) ?? undefined;
  }
}
