import { injectable } from 'inversify';

@injectable()
export abstract class Logger {
  abstract log(message: string): void;

  abstract error(message: string): void;
}

@injectable()
export class DefaultLogger extends Logger {
  error(message: string): void {
    /* eslint-disable no-console */
    console.error(message);
  }

  log(message: string): void {
    /* eslint-disable no-console */
    console.info(message);
  }
}
