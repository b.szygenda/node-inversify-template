import { interfaces } from 'inversify';

export function getAllDependencies(container: interfaces.Container): Map<any, any> {
  return Reflect.get(Reflect.get(container, '_bindingDictionary'), '_map');
}
