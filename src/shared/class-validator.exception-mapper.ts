import { ValidationError } from 'class-validator';
import { ValidationException } from './exceptions/validation.exception';
import { Errors } from './errors';

export class ClassValidatorExceptionMapper {
  map(validationErrors: ValidationError[]): ValidationException[] {
    const constraintsErrorsFromAllChildren = validationErrors.reduce((acc, error) => {
      const mapped = this.getConstraintsFromChildren(error);

      if (mapped.length > 0) {
        acc.push(mapped.flat());
      }

      return acc;
    }, [] as ValidationException[][]);

    const constraintsErrorsFromParents = validationErrors.reduce((acc, error) => {
      if (error.constraints) {
        acc.push(this.mapFromConstraint(error));
      }

      return acc;
    }, [] as ValidationException[][]);

    return [...constraintsErrorsFromAllChildren.flat(), ...constraintsErrorsFromParents.flat()];
  }

  private getConstraintsFromChildren(validationErrors: ValidationError): ValidationException[][] {
    return (validationErrors.children || []).reduce((acc, child) => {
      if (child.constraints) {
        acc.push(this.mapFromConstraint(child, validationErrors.property).flat());
      }

      if (child.children?.length) {
        acc.push(this.getConstraintsFromChildren(child).flat());
      }

      return acc;
    }, [] as ValidationException[][]);
  }

  private mapFromConstraint(errorValidation: ValidationError, parentPropertyName?: string): ValidationException[] {
    if (!errorValidation.constraints) {
      return [];
    }

    const property: string = parentPropertyName ? `${parentPropertyName}.${errorValidation.property}` : errorValidation.property;

    return Object.entries(errorValidation.constraints).map(([_, errorMessage]) => ({
      value: errorValidation.value,
      param: property,
      location: 'body',
      msg: errorMessage,
      errorCode: errorValidation.contexts?.errorCode ?? Errors.validationFailed,
    }));
  }
}
