import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { Errors } from '../errors';
import { UnprocessableEntityException } from '../exceptions';

export const validateMiddleware = (req: Request, res: Response, next: NextFunction): void => {
  const errors = validationResult(req).formatWith((error) => ({
    errorCode: parseInt(error.msg, 10) || Errors.validationFailed,
    field: error.param,
  }));

  if (!errors.isEmpty()) {
    throw new UnprocessableEntityException({
      errorCode: Errors.validationFailed,
      details: errors.array(),
    });
  }
  next();
};
