import { injectable } from 'inversify';
import { ApiResponse, HttpMethods, HttpRequestConfig } from './http-client.types';

@injectable()
export abstract class HttpClient {
  abstract sendRequest<TResponseData>({
    url,
    method,
    config,
  }: {
    url: string;
    method: HttpMethods;
    config?: HttpRequestConfig;
  }): Promise<ApiResponse<TResponseData>>;
}
