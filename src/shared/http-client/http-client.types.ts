export enum HttpMethods {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
  PATCH = 'PATCH',
  PUT = 'PUT',
  OPTIONS = 'OPTIONS',
}

export interface HttpRequestConfig {
  query?: Record<string, any>;
  headers?: Record<string, any>;
  body?: Record<string, any>;
  timeout?: number;
}

export interface ApiResponse<TResponseData> {
  headers: Record<string, any>;
  data: TResponseData;
}
