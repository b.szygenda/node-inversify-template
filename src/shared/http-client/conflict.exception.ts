import { StatusCodes } from 'http-status-codes';
import { HttpException } from '../../shared/exceptions';

export class ConflictException extends HttpException {
  constructor(response: Record<string, any>) {
    super(response, StatusCodes.CONFLICT);
  }
}
