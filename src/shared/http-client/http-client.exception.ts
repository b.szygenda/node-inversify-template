import { StatusCodes } from 'http-status-codes';
import { ExceptionConstructor, HttpException } from '../../shared/exceptions';
import { Errors } from '../errors';

export class HttpClientException extends HttpException {
  static createHttpClientException(opts?: {
    statusCode: StatusCodes;
    errorCode: Errors;
    message: string;
    details: Array<{
      errorCode: Errors;
      message?: string;
      field?: string;
    }>;
    previousException?: Error | any;
  }): HttpClientException {
    return new HttpClientException({
      status: opts?.statusCode ?? StatusCodes.INTERNAL_SERVER_ERROR,
      message: opts?.message ?? 'HTTP error occurred',
      errorCode: opts?.errorCode ?? Errors.serverError,
      details: opts?.details,
      previousException: opts?.previousException,
    });
  }

  private constructor({ errorCode, message, status, details, previousException }: ExceptionConstructor) {
    super(
      HttpException.createBody(
        {
          errorCode,
          message,
          details,
        },
        status
      ),
      status,
      previousException
    );
  }
}
