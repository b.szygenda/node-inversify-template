import axios from 'axios';
import { HttpClient } from './http-client';
import { HttpClientException } from './http-client.exception';
import { ApiResponse, HttpMethods, HttpRequestConfig } from './http-client.types';

export class AxiosHttpClient extends HttpClient {
  async sendRequest<TResponseData>({
    url,
    method,
    config,
  }: {
    url: string;
    method: HttpMethods;
    config?: HttpRequestConfig;
  }): Promise<ApiResponse<TResponseData>> {
    try {
      const response = await axios.request({
        url,
        params: config?.query,
        method,
        headers: config?.headers,
        data: config?.body,
        timeout: config?.timeout,
      });

      return {
        data: response.data,
        headers: response.headers,
      };
    } catch (e: any) {
      const errorBody = e?.response?.data;

      throw HttpClientException.createHttpClientException({
        statusCode: e?.response?.status,
        message: e?.response?.statusText,
        ...errorBody,
        previousException: e,
      });
    }
  }
}
