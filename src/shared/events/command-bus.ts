import { injectable } from 'inversify';
import { Command } from './command';
import { CommandHandler } from './command.handler';

@injectable()
export class CommandBus {
  constructor(private readonly commands: ReadonlyMap<string, CommandHandler<Command<unknown>>>) {}

  async handle(command: Command<unknown>) {
    const handler = this.commands.get(command.constructor.name);

    if (!handler) {
      throw new Error(`Command bus handler: ${command.constructor.name} not found.`);
    }

    await handler.execute(command);
  }
}
