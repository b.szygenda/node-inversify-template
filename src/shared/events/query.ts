export abstract class Query<T> {
  constructor(props: T) {
    Object.assign(this, props);
  }
}
