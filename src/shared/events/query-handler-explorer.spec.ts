import { Container, injectable } from 'inversify';
import { QueryHandlerExplorer, RegisterQueryHandler } from './query-handler-explorer';

class FirstQuery {}

@RegisterQueryHandler(FirstQuery)
@injectable()
class FirstQueryHandler {}

class SecondQuery {}

@RegisterQueryHandler(SecondQuery)
@injectable()
class SecondQueryHandler {}

@injectable()
class TestService {}

describe('QueryHandlerExplorer', () => {
  let container: Container;

  beforeEach(() => {
    container = new Container();
    container.bind(FirstQueryHandler).to(FirstQueryHandler).inSingletonScope();
    container.bind(SecondQueryHandler).to(SecondQueryHandler).inSingletonScope();
    container.bind(TestService).to(TestService).inSingletonScope();
    container
      .bind(QueryHandlerExplorer)
      .toDynamicValue(({ container: contextContainer }) => QueryHandlerExplorer.create(contextContainer))
      .inSingletonScope();
  });

  it('should found command handler in IOC Container', () => {
    const queryHandlerExplorer = container.get(QueryHandlerExplorer);

    expect(queryHandlerExplorer.queryHandlers.size).toBe(2);
    expect(Array(...queryHandlerExplorer.queryHandlers)).toMatchObject([
      [FirstQuery.name, expect.any(FirstQueryHandler)],
      [SecondQuery.name, expect.any(SecondQueryHandler)],
    ]);
  });
});
