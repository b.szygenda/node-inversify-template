import { injectable, interfaces } from 'inversify';
import { CommandHandler } from './command.handler';
import { isMetadataGettable } from '../metadata-utils';
import { getAllDependencies } from '../ioc-utils';

export const COMMAND_HANDLER_REGISTER_METADATA = Symbol('COMMAND_HANDLER_REGISTER_METADATA');

export function RegisterCommandHandler(command: Function) {
  return (constructor: Function) => {
    Reflect.defineMetadata(COMMAND_HANDLER_REGISTER_METADATA, command.name, constructor);
  };
}

@injectable()
export class CommandHandlerExplorer {
  static create(container: interfaces.Container): CommandHandlerExplorer {
    const commandConfig = new Map<string, CommandHandler<any>>();

    const dependencies = getAllDependencies(container);

    dependencies.forEach((_value: any, serviceKey: any) => {
      if (!isMetadataGettable(serviceKey)) {
        return;
      }

      const command = Reflect.getMetadata(COMMAND_HANDLER_REGISTER_METADATA, serviceKey);

      if (command) {
        commandConfig.set(command, container.get<CommandHandler<any>>(serviceKey));
      }
    });

    return new CommandHandlerExplorer(commandConfig);
  }

  constructor(readonly commandHandlers: ReadonlyMap<string, CommandHandler<any>>) {}
}
