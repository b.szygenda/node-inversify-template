import { Container, injectable } from 'inversify';
import { CommandHandlerExplorer, RegisterCommandHandler } from './command-handler-explorer';

class FirstCommand {}

@RegisterCommandHandler(FirstCommand)
@injectable()
class FirstCommandHandler {}

class SecondCommand {}

@RegisterCommandHandler(SecondCommand)
@injectable()
class SecondCommandHandler {}

@injectable()
class TestService {}

describe('CommandHandlerExplorer', () => {
  let container: Container;

  beforeEach(() => {
    container = new Container();
    container.bind(FirstCommandHandler).to(FirstCommandHandler).inSingletonScope();
    container.bind(SecondCommandHandler).to(SecondCommandHandler).inSingletonScope();
    container.bind(TestService).to(TestService).inSingletonScope();
    container
      .bind(CommandHandlerExplorer)
      .toDynamicValue(({ container: contextContainer }) => CommandHandlerExplorer.create(contextContainer))
      .inSingletonScope();
  });

  it('should found command handler in IOC Container', () => {
    const commandHandlerExplorer = container.get(CommandHandlerExplorer);

    expect(commandHandlerExplorer.commandHandlers.size).toBe(2);
    expect(Array(...commandHandlerExplorer.commandHandlers)).toMatchObject([
      [FirstCommand.name, expect.any(FirstCommandHandler)],
      [SecondCommand.name, expect.any(SecondCommandHandler)],
    ]);
  });
});
