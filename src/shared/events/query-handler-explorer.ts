import { injectable, interfaces } from 'inversify';
import { QueryHandler } from './query-handler';
import { isMetadataGettable } from '../metadata-utils';
import { getAllDependencies } from '../ioc-utils';

export const QUERY_HANDLER_REGISTER_METADATA = Symbol('QUERY_HANDLER_REGISTER_METADATA');

export function RegisterQueryHandler(query: Function) {
  return (constructor: Function) => {
    Reflect.defineMetadata(QUERY_HANDLER_REGISTER_METADATA, query.name, constructor);
  };
}

@injectable()
export class QueryHandlerExplorer {
  static create(container: interfaces.Container): QueryHandlerExplorer {
    const queriesConfig = new Map<string, QueryHandler<any, any>>();

    const dependencies = getAllDependencies(container);

    dependencies.forEach((_value: any, serviceKey: any) => {
      if (!isMetadataGettable(serviceKey)) {
        return;
      }

      const query = Reflect.getMetadata(QUERY_HANDLER_REGISTER_METADATA, serviceKey);

      if (query) {
        queriesConfig.set(query, container.get<QueryHandler<any, any>>(serviceKey));
      }
    });

    return new QueryHandlerExplorer(queriesConfig);
  }

  constructor(readonly queryHandlers: ReadonlyMap<string, QueryHandler<any, any>>) {}
}
