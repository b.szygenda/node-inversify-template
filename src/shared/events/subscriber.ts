export interface Subscriber<TEvent> {
  subscribe(publishedEvent: TEvent): Promise<void>;
}

export class NullObjectSubscriber implements Subscriber<any> {
  subscribe(_publishedEvent: any): Promise<void> {
    return Promise.resolve(undefined);
  }
}
