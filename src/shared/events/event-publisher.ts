import { captureException } from '@sentry/node';
import { injectable } from 'inversify';
import { Logger } from '../services/logger/logger';
import { PublishableAggregate } from './aggregate-root';
import { BasicEvent } from './basic.event';
import { Subscriber } from './subscriber';
import { inspect } from 'util';
import { isNil } from '../exceptions';

@injectable()
export class EventPublisher {
  constructor(
    private readonly subscribers: Map<string, Array<Subscriber<any>>>,
    private readonly eventStorageSubscriber: Subscriber<any>,
    private readonly logger: Logger,
    private readonly globalSubscribers: Subscriber<any>[]
  ) {}

  mergeObjectContext<TEntity extends PublishableAggregate>(entity?: TEntity): void {
    if (isNil(entity)) {
      return;
    }
    Object.assign(entity, { eventPublisher: this });
  }

  async commit(eventsToCommit: BasicEvent[], subscribers: Map<string, Array<Subscriber<any>>>): Promise<void> {
    await Promise.all(
      eventsToCommit.map(async (event, index) => {
        try {
          const eventSubscribers = [this.eventStorageSubscriber, ...this.globalSubscribers, ...this.getSubscribers(event, subscribers)];

          await Promise.all(
            eventSubscribers.map(async (subscriber) =>
              this.handleSubscription({
                subscriber,
                event,
              })
            )
          );
        } finally {
          eventsToCommit.splice(index, 1);
        }
      })
    );
  }

  private getSubscribers(event: BasicEvent, subscribers: Map<string, Array<Subscriber<any>>>): Array<Subscriber<any>> {
    const localSubscribers = subscribers.get(event.constructor.name) ?? [];
    const globalSubscribers = this.subscribers.get(event.constructor.name) ?? [];

    return [...localSubscribers, ...globalSubscribers];
  }

  private async handleSubscription({ event, subscriber }: { subscriber: Subscriber<any>; event: BasicEvent }): Promise<void> {
    try {
      await subscriber.subscribe(event);
    } catch (e) {
      captureException(e);
      this.logger.error(inspect(e));
    }
  }
}
