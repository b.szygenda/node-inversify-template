export interface BasicEvent {
  readonly entityName: string;
  readonly userId: string | null;
  readonly createdAt: Date;
}
