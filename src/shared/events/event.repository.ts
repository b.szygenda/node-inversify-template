import { injectable } from 'inversify';
import { BaseRepository } from '../repositories/base.repository';
import { PaginationOptions, PaginationResult } from '../repositories/pagination';
import { Event } from './event.entity';

@injectable()
export abstract class BaseEventRepository extends BaseRepository<Event<any>> {
  async save<TEventData>(event: Event<TEventData>): Promise<void> {
    await this.repository.save(event);
  }

  findByUserId({ paginateOptions, userId }: { paginateOptions: PaginationOptions; userId: string }): Promise<PaginationResult<Event>> {
    const qb = this.repository.createQueryBuilder('e').where(`e.data ->> 'userId' = :userId`, { userId }).orderBy('e.createdAt', 'DESC');

    return this.paginate(paginateOptions, qb);
  }
}
