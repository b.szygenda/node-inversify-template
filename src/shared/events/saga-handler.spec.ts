import { Container, interfaces } from 'inversify';
import { Logger } from '../services/logger/logger';
import { CommandBus } from './command-bus';
import { SagaHandler } from './saga-handler';

class TestEvent {}

class TestCommand {}

describe('SagaHandler', () => {
  let commandBus: CommandBus;
  let logger: Logger;
  let container: interfaces.Container;
  const sagaService = { sagaMethod: jest.fn() };
  const sagas = [
    {
      sagaService,
      methodKey: 'sagaMethod',
    },
  ];

  beforeEach(() => {
    jest.resetAllMocks();
    commandBus = {
      handle: jest.fn(),
    } as unknown as CommandBus;
    logger = {
      log: jest.fn(),
    } as unknown as Logger;
    container = new Container();
    container.bind(CommandBus).toConstantValue(commandBus);
  });

  it('should invoke saga', async () => {
    const sagaHandler = new SagaHandler(sagas, container, logger);

    await sagaHandler.subscribe(new TestEvent());

    expect(sagaService.sagaMethod).toBeCalledWith(expect.any(TestEvent));
  });

  it('should call command bus with command', async () => {
    jest.spyOn(sagaService, 'sagaMethod').mockResolvedValueOnce(new TestCommand());
    const sagaHandler = new SagaHandler(sagas, container, logger);

    await sagaHandler.subscribe(new TestEvent());

    expect(commandBus.handle).toBeCalledWith(expect.any(TestCommand));
  });

  it('should not call command bus, when saga doesnt return command', async () => {
    jest.spyOn(sagaService, 'sagaMethod').mockResolvedValueOnce(undefined);
    const sagaHandler = new SagaHandler(sagas, container, logger);

    await sagaHandler.subscribe(new TestEvent());

    expect(commandBus.handle).toBeCalledTimes(0);
  });
});
