import { injectable } from 'inversify';
import { BaseEventFactory } from './base-event-factory';
import { BaseEventRepository } from './event.repository';
import { Subscriber } from './subscriber';

@injectable()
export class EventStorageSubscriber implements Subscriber<any> {
  constructor(private readonly eventRepository: BaseEventRepository, private readonly eventFactory: BaseEventFactory) {}

  async subscribe(publishedEvent: any): Promise<void> {
    const event = this.eventFactory.create(publishedEvent);

    await this.eventRepository.save(event);
  }
}
