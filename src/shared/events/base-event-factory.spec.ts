import * as faker from 'faker';
import { BaseEventFactory } from './base-event-factory';
import { BasicEvent } from './basic.event';
import { Event } from './event.entity';
import { ClassConstructor } from 'class-transformer';

class TestEventEntity implements Event {
  readonly id!: string;
  readonly entity!: string;
  readonly sequence!: number;
  readonly type!: string;
  readonly createdAt!: Date;
  readonly data!: Record<any, any>;
  constructor(partial: Partial<Event>, _second: any) {
    Object.assign(this, partial);
  }
}

class TestEventFactory extends BaseEventFactory {
  protected readonly eventEntity: ClassConstructor<Event> = TestEventEntity;
}

class TestEventCreated implements BasicEvent {
  readonly createdAt: Date = new Date();
  readonly entityName: string = TestEventEntity.name;
  readonly userId: string = faker.datatype.uuid();
}

describe('BaseEventFactory unit tests', () => {
  it('should create instance of TestEvent', () => {
    const eventFactory = new TestEventFactory();

    const result = eventFactory.create(new TestEventCreated());

    expect(result).toBeInstanceOf(TestEventEntity);
    expect(result).toMatchObject({
      data: {
        createdAt: expect.any(Date),
        entityName: 'TestEventEntity',
        userId: expect.any(String),
      } as BasicEvent,
      type: 'TestEventCreated',
      entity: 'TestEventEntity',
    });
  });
});
