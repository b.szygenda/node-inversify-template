import * as faker from 'faker';
import { QueryBus } from './query-bus';
import { QueryHandler } from './query-handler';

describe('Query bus unit tests', () => {
  let queryBus: QueryBus;
  const mock: jest.Mock = jest.fn();

  class TestQuery {
    test!: string;

    constructor(partial: Partial<TestQuery>) {
      Object.assign(this, partial);
    }
  }

  class TestQueryHandler implements QueryHandler<TestQuery, any> {
    execute(data: TestQuery): any {
      return mock(data);
    }
  }

  beforeEach(async () => {
    jest.resetAllMocks();
    const commandConfig = new Map<string, QueryHandler<any, any>>([[TestQuery.name, new TestQueryHandler()]]);

    queryBus = new QueryBus(commandConfig);
  });

  it('Should call a query handler', async () => {
    const query = new TestQuery({ test: faker.random.word() });

    await queryBus.handle(query);

    expect(mock).toHaveBeenCalledWith(query);
  });

  it('Should return value', async () => {
    const returnValue = faker.random.word();
    const query = new TestQuery({ test: faker.random.word() });

    mock.mockReturnValueOnce(returnValue);

    const result = await queryBus.handle(query);

    expect(result).toEqual(returnValue);
  });

  it('Should throw', async () => {
    const command = { test: faker.random.word() };

    await expect(queryBus.handle(command)).rejects.toThrow(Error);
  });
});
