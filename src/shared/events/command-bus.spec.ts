import * as faker from 'faker';
import { CommandBus } from './command-bus';
import { CommandHandler } from './command.handler';

describe('Command bus unit tests', () => {
  let commandBus: CommandBus;
  const mock: jest.Mock = jest.fn();

  class TestCommand {
    test!: string;

    constructor(partial: Partial<TestCommand>) {
      Object.assign(this, partial);
    }
  }

  class TestCommandHandler implements CommandHandler<TestCommand> {
    execute(data: TestCommand): void {
      mock(data);
    }
  }

  beforeEach(async () => {
    jest.resetAllMocks();
    const commandConfig = new Map<string, CommandHandler<any>>([[TestCommand.name, new TestCommandHandler()]]);

    commandBus = new CommandBus(commandConfig);
  });

  it('Should call a command handler', async () => {
    const command = new TestCommand({ test: faker.random.word() });

    await commandBus.handle(command);

    expect(mock).toHaveBeenCalledWith(command);
  });

  it('Should throw', async () => {
    const command = { test: faker.random.word() };

    await expect(commandBus.handle(command)).rejects.toThrow(Error);
  });
});
