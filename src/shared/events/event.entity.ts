export interface Event<TEventData = Record<any, any>> {
  readonly id: string;
  readonly sequence: number;
  readonly data: TEventData;
  readonly type: string;
  readonly entity: string;
  readonly createdAt: Date;
}

export interface EventConstructor {
  new (partial: Partial<Event>): Event;
}
