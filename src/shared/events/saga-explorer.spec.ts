import { Container, injectable } from 'inversify';
import { Saga, SagaCallable, SagaExplorer } from './saga-explorer';

@injectable()
class SomeSagaService {
  @Saga()
  sagaMethod() {
    throw new Error();
  }

  regularMethod() {
    throw new Error();
  }

  @Saga()
  secondSagaMethod() {
    throw new Error();
  }
}

@injectable()
class SecondSagaService {
  @Saga()
  sagaMethodFromSecondService() {
    throw new Error();
  }

  regularMethodFromSecondService() {
    throw new Error();
  }
}

describe('SagaExplorer', () => {
  let container: Container;

  beforeEach(() => {
    container = new Container();
    container.bind(SomeSagaService).to(SomeSagaService).inSingletonScope();
    container.bind(SecondSagaService).to(SecondSagaService).inSingletonScope();
    container.bind('SecondSagaService').to(SecondSagaService).inSingletonScope();
    container
      .bind(SagaExplorer)
      .toDynamicValue(({ container: contextContainer }) => SagaExplorer.create(contextContainer))
      .inSingletonScope();
  });

  it('should found sagas in IOC Container', () => {
    const sagaExplorer = container.get(SagaExplorer);

    const result = sagaExplorer.sagas;

    expect(result.length).toBe(3);
    expect(result).toMatchObject(
      expect.arrayContaining([
        {
          sagaService: expect.any(SomeSagaService),
          methodKey: 'sagaMethod' as keyof SomeSagaService,
        } as SagaCallable,
        {
          sagaService: expect.any(SomeSagaService),
          methodKey: 'secondSagaMethod' as keyof SomeSagaService,
        } as SagaCallable,
        {
          sagaService: expect.any(SecondSagaService),
          methodKey: 'sagaMethodFromSecondService' as keyof SecondSagaService,
        } as SagaCallable,
      ])
    );
  });
});
