export interface CommandHandler<T> {
  execute(data: T): Promise<void> | void;
}
