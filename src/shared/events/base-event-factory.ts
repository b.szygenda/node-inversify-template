import { injectable } from 'inversify';
import { BasicEvent } from './basic.event';
import { Event, EventConstructor } from './event.entity';
import { ClassConstructor } from 'class-transformer';

@injectable()
export abstract class BaseEventFactory {
  protected abstract readonly eventEntity: ClassConstructor<Event>;

  create(publishedEvent: BasicEvent): Event {
    return new (this.eventEntity as EventConstructor)({
      data: publishedEvent,
      type: publishedEvent.constructor.name,
      entity: publishedEvent.entityName,
    });
  }
}
