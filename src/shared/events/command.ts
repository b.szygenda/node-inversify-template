export abstract class Command<T> {
  constructor(props: T) {
    Object.assign(this, props);
  }
}
