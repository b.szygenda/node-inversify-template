import { captureException } from '@sentry/node';
import { injectable, interfaces } from 'inversify';
import { inspect } from 'util';
import { Subscriber } from './subscriber';
import { Logger } from '../services/logger/logger';
import { CommandBus } from './command-bus';
import { SagaCallable } from './saga-explorer';

/**
 * We are injecting container because,we have problem while resolving 'toDynamicValue' in inversify container.
 * Problem occurs between EventPublisher, CommandBus and SagaHandler classes
 */
@injectable()
export class SagaHandler implements Subscriber<any> {
  constructor(private readonly sagas: SagaCallable[], private readonly container: interfaces.Container, private readonly logger: Logger) {}

  async subscribe(publishedEvent: any): Promise<void> {
    const commandBus = this.container.get(CommandBus);

    await Promise.all(
      this.sagas.map(async ({ sagaService, methodKey }) => {
        const eventName = publishedEvent.constructor.name;

        try {
          const saga = `${sagaService.constructor.name}.${methodKey}()`;

          this.logger.log(`Saga: ${saga} is running for ${eventName} event`);

          const command = await sagaService[methodKey](publishedEvent);

          if (command) {
            this.logger.log(`Saga: ${saga} returns ${command?.constructor?.name} command`);
            await commandBus.handle(command);
          }
        } catch (e) {
          captureException(e);
          this.logger.error(`Error occurred while processing saga: ${sagaService}${methodKey} with ${eventName}`);
          this.logger.error(inspect(e));
        }
      })
    );
  }
}
