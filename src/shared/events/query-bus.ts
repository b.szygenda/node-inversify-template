import { injectable } from 'inversify';
import { QueryHandler } from './query-handler';

@injectable()
export class QueryBus {
  constructor(private readonly queries: ReadonlyMap<string, QueryHandler<Record<string, any>, any>>) {}

  async handle<TQueryResult>(command: Record<string, any>): Promise<TQueryResult> {
    const handler = this.queries.get(command.constructor.name);

    if (!handler) {
      throw new Error(`Query bus handler: ${command.constructor.name} not found.`);
    }

    return handler.execute(command);
  }
}
