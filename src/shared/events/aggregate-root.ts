import { BasicEvent } from './basic.event';
import { EventPublisher } from './event-publisher';
import { Subscriber } from './subscriber';
import { ClassConstructor } from 'class-transformer';

export interface PublishableAggregate {
  publish<TEvent extends BasicEvent>(event: TEvent): void;
  addSubscribers<TEvent, TEventSubscriber = TEvent & BasicEvent>(
    event: ClassConstructor<TEvent>,
    subscribers: Array<Subscriber<TEventSubscriber>>
  ): void;
  commit(): Promise<void>;
}

export abstract class AggregateRoot implements PublishableAggregate {
  protected readonly eventPublisher!: EventPublisher;
  private subscribers: Map<string, Array<Subscriber<any>>>;
  private eventsToCommit: BasicEvent[] = [];

  protected constructor() {
    this.subscribers = new Map<string, Array<Subscriber<any>>>();
  }

  publish<TEvent extends BasicEvent>(event: TEvent): void {
    this.eventsToCommit.push(event);
  }

  addSubscribers<TEvent, TEventSubscriber = TEvent & BasicEvent>(
    event: ClassConstructor<TEvent>,
    subscribers: Array<Subscriber<TEventSubscriber>>
  ): void {
    const existingSubscribers = this.subscribers.has(event.name) ? this.subscribers.get(event.name) : [];

    const getAllSubscribers = () => {
      if (existingSubscribers) {
        return subscribers.concat(existingSubscribers);
      }

      return subscribers;
    };

    this.subscribers.set(event.name, getAllSubscribers());
  }

  async commit(): Promise<void> {
    await this.eventPublisher.commit(this.eventsToCommit, this.subscribers);
    this.eventsToCommit = [];
    this.subscribers = new Map<string, Array<Subscriber<any>>>();
  }
}
