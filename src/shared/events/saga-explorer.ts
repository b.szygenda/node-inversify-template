import { injectable, interfaces } from 'inversify';
import { isMetadataGettable } from '../metadata-utils';

export const SAGA_METADATA = Symbol('SAGA_METADATA');

export function Saga() {
  // eslint-disable-next-line func-names
  return function (target: Record<any, any>, propertyKey: string) {
    const properties = Reflect.getOwnMetadata(SAGA_METADATA, target.constructor) ?? [];

    Reflect.defineMetadata(SAGA_METADATA, [...properties, propertyKey], target.constructor);
  };
}

export type SagaCallable = { sagaService: Record<any, any>; methodKey: string };

@injectable()
export class SagaExplorer {
  static create(container: interfaces.Container): SagaExplorer {
    const allDependencies: Map<any, any> = Reflect.get(Reflect.get(container, '_bindingDictionary'), '_map');

    const sagas: SagaCallable[] = [];

    allDependencies.forEach((_value, serviceKey) => {
      if (!isMetadataGettable(serviceKey)) {
        return;
      }

      const properties = Reflect.getMetadata(SAGA_METADATA, serviceKey) || [];

      properties.forEach((property: string) => {
        sagas.push({
          sagaService: container.get(serviceKey),
          methodKey: property,
        });
      });
    });

    return new SagaExplorer(sagas);
  }

  constructor(private readonly _sagas: SagaCallable[]) {}

  get sagas(): SagaCallable[] {
    return this._sagas;
  }
}
