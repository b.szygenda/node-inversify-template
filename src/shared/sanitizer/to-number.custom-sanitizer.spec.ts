import * as faker from 'faker';
import { ToNumberCustomSanitizer } from './to-number.custom-sanitizer';

describe('ToNumberCustomSanitizer unit tests', () => {
  it.each([
    {
      value: '1',
      expectedValue: 1,
    },
    {
      value: 0,
      expectedValue: 0,
    },
    {
      value: faker.datatype.string(),
      expectedValue: NaN,
    },
  ])('Should transform to appropriate number', ({ value, expectedValue }) => {
    const result = ToNumberCustomSanitizer(value);

    expect(result).toBe(expectedValue);
  });
});
