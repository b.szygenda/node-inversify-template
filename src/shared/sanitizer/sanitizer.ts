/* eslint-disable no-param-reassign */
import { NextFunction, Request, Response } from 'express';
import { isObject, isString } from '../exceptions';
import { Middleware } from '../middleware/middleware';

export class Sanitizer {
  static initializeMiddleware(sanitizers: Array<(value: string) => string>): Middleware {
    const sanitizer = new Sanitizer(sanitizers);

    return (req: Request, res: Response, next: NextFunction): void => {
      const body = sanitizer.sanitize(req.body);

      Object.assign(req, { body });

      next();
    };
  }

  constructor(private readonly sanitizers: Array<(value: string) => string>) {}

  sanitize(payload: unknown): unknown {
    if (isObject(payload)) {
      return this.sanitizeObject(payload);
    }

    if (isString(payload)) {
      return this.sanitizeString(payload);
    }

    return payload;
  }

  private sanitizeObject(payload: any): unknown {
    Object.keys(payload).forEach((key) => {
      if (isObject(payload[key])) {
        payload[key] = this.sanitizeObject(payload[key]);
      } else if (isString(payload[key])) {
        payload[key] = this.sanitizeString(payload[key]);
      }
    });

    return payload;
  }

  private sanitizeString(string: string): string {
    return this.sanitizers.reduce((sanitizedString, sanitizer: (value: string) => string) => sanitizer(sanitizedString), string);
  }
}
