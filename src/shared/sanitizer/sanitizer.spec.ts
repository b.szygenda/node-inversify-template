import faker from 'faker';
import { Sanitizer } from './sanitizer';
import { trimmer } from './trimmer';
import { xssParser } from './xss.parser';

describe('Sanitizer unit tests', () => {
  let sanitizer: Sanitizer;

  describe('xss tests', () => {
    beforeEach(() => {
      sanitizer = new Sanitizer([xssParser]);
    });

    it('Should parse regular string', () => {
      const str = faker.random.word();

      const result = sanitizer.sanitize(str);

      expect(result).toBe(str);
    });

    it('Should sanitize vulnerable string', () => {
      const str = "<script>alert('sanitized')</script>";

      const result = sanitizer.sanitize(str);

      expect(result).toBe("&lt;script&gt;alert('sanitized')&lt;/script&gt;");
    });

    it('Should parse regular object', () => {
      const obj = {
        [faker.random.word()]: {
          [faker.random.word()]: faker.datatype.number(),
        },
      };

      const result = sanitizer.sanitize(obj);

      expect(result).toMatchObject(obj);
    });

    it('Should sanitize vulnerable object', () => {
      const obj = {
        a: {
          b: '<button>vulnerable</button>',
        },
      };

      const result = sanitizer.sanitize(obj);

      expect(result).toMatchObject({
        a: {
          b: '&lt;button&gt;vulnerable&lt;/button&gt;',
        },
      });
    });

    it.each([undefined, null, {}, '', 0, false])('Should handle empty payloads', (empty) => {
      const result = sanitizer.sanitize(empty);

      expect(result).toBe(empty);
    });
  });

  describe('trimmer tests', () => {
    beforeEach(() => {
      sanitizer = new Sanitizer([trimmer]);
    });

    it('Should trim string', () => {
      const expected = faker.datatype.string();
      const given = ` ${expected} `;

      expect(sanitizer.sanitize(given)).toBe(expected);
    });

    it.each([faker.datatype.string(), faker.datatype.number(), {}, null, undefined, false, 0])('Should not trim', (value) => {
      expect(sanitizer.sanitize(value)).toBe(value);
    });

    it('should not trim nested object', () => {
      const given = {
        nested: {
          notTrimmed: faker.datatype.string(),
        },
      };

      expect(sanitizer.sanitize(given)).toMatchObject(given);
    });

    it('Should trim nested object', () => {
      const expectedString = faker.datatype.string();

      const given = {
        nested: {
          trimmed: ` ${expectedString} `,
        },
      };

      expect(sanitizer.sanitize(given)).toMatchObject({
        nested: {
          trimmed: expectedString,
        },
      });
    });
  });

  describe('two sanitizations test', () => {
    beforeEach(() => {
      sanitizer = new Sanitizer([xssParser, trimmer]);
    });

    it('Should trim and parse xss', () => {
      const given = {
        parsed: ` <script>alert('sanitized')</script> `,
      };

      expect(sanitizer.sanitize(given)).toMatchObject({
        parsed: "&lt;script&gt;alert('sanitized')&lt;/script&gt;",
      });
    });
  });
});
