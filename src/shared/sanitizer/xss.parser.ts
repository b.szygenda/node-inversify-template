const xss = require('xss');

export const xssParser = (value: string) => xss(value);
