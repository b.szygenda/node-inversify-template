import { getConnectionManager } from 'typeorm';
import { DatabaseSession } from './database-session';
import { DatabaseSessionException } from './database-session.exception';
import { AsyncLocalStorage } from '../async-local-storage';

export class DatabaseSessionManager {
  static initDatabaseSession(): void {
    const connectionManager = getConnectionManager();

    if (!connectionManager?.connections?.length) {
      throw DatabaseSessionException.createConnectionNotEstablishedException();
    }

    connectionManager.connections.forEach((connection) => {
      AsyncLocalStorage.set(DatabaseSessionManager.composeDatabaseSessionProviderName(connection.name), new DatabaseSession(connection));
    });
  }

  static getDatabaseSession(connectionName: string = 'default'): DatabaseSession {
    const databaseSession = AsyncLocalStorage.get<DatabaseSession>(DatabaseSessionManager.composeDatabaseSessionProviderName(connectionName));

    if (!databaseSession) {
      throw DatabaseSessionException.createCannotFindDatabaseSession(connectionName);
    }

    return databaseSession;
  }

  private static composeDatabaseSessionProviderName(connectionName: string): string {
    return `DATABASE_SESSION_CONNECTION_${connectionName}`;
  }

  private constructor() {}
}
