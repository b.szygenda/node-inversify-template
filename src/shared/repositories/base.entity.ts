import { EntitySchemaColumnOptions } from 'typeorm';
import { EntitySchemaRelationOptions } from 'typeorm/entity-schema/EntitySchemaRelationOptions';

export interface IBaseEntityId {
  id: string;
}

export interface IBaseEntityDateCreatedDate {
  createdDate: Date;
}

export interface IBaseEntityDateUpdatedDate {
  updatedDate: Date;
}

export interface IBaseEntityDateDeletedDate {
  deletedDate: Date | null;
}

export interface IBaseEntityDates extends IBaseEntityDateCreatedDate, IBaseEntityDateUpdatedDate, IBaseEntityDateDeletedDate {}

export const RelationBlockPersistenceEntitySchema: Readonly<Pick<EntitySchemaRelationOptions, 'cascade' | 'persistence'>> = {
  cascade: false,
  persistence: false,
};

export const BaseCreatedDateColumnEntitySchema: Readonly<Record<keyof IBaseEntityDateCreatedDate, EntitySchemaColumnOptions>> = {
  createdDate: {
    name: 'created_date',
    createDate: true,
  } as EntitySchemaColumnOptions,
};

export const BaseUpdatedDateColumnEntitySchema: Readonly<Record<keyof IBaseEntityDateUpdatedDate, EntitySchemaColumnOptions>> = {
  updatedDate: {
    name: 'updated_date',
    updateDate: true,
  } as EntitySchemaColumnOptions,
};

export const BaseDeletedDateColumnEntitySchema: Readonly<Record<keyof IBaseEntityDateDeletedDate, EntitySchemaColumnOptions>> = {
  deletedDate: {
    name: 'deleted_date',
    nullable: true,
    deleteDate: true,
  } as EntitySchemaColumnOptions,
};

export const BaseDatesColumnsEntitySchema: Readonly<Record<keyof IBaseEntityDates, EntitySchemaColumnOptions>> = {
  ...BaseCreatedDateColumnEntitySchema,
  ...BaseUpdatedDateColumnEntitySchema,
  ...BaseDeletedDateColumnEntitySchema,
};
