import { FindManyOptions, FindOneOptions } from 'typeorm';

export interface IRepository<I, E> {
  get(uuid: string, options?: FindOneOptions): Promise<I | E>;
  getOne(options: FindOneOptions): Promise<I | E>;
  getAll(options?: FindManyOptions): Promise<I[]>;
  add(entity: I): Promise<I | E>;
  update(entity: I): Promise<I | E>;
  delete(uuid: string): Promise<boolean>;
}
