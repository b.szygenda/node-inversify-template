import { injectable } from 'inversify';
import { getRepository, QueryBuilder, Repository, SelectQueryBuilder } from 'typeorm';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';
import { DatabaseSessionManager } from './database-session.manager';
import { PaginationOptions, PaginationResult } from './pagination';
import { ClassConstructor } from 'class-transformer';

@injectable()
export abstract class BaseRepository<TEntity> {
  protected abstract readonly entity: ClassConstructor<TEntity>;

  protected constructor() {}

  protected get repository(): Repository<TEntity> {
    return this.getRepository(this.entity);
  }

  protected getRepository<TRepository = TEntity>(entity?: ClassConstructor<TRepository>): Repository<TRepository> {
    const databaseSession = DatabaseSessionManager.getDatabaseSession();

    return databaseSession.getRepository<TRepository>(entity ?? this.entity);
  }

  protected async paginate(
    paginateOptions: PaginationOptions,
    query?: Omit<FindManyOptions<TEntity>, 'skip' | 'take'> | SelectQueryBuilder<TEntity>
  ): Promise<PaginationResult<TEntity>> {
    if (query instanceof QueryBuilder) {
      const [entities, count] = await query.skip(paginateOptions.offset).take(paginateOptions.limit).getManyAndCount();

      return {
        count,
        entities,
      };
    }

    const [entities, count] = await getRepository<TEntity>(this.entity).findAndCount({
      ...query,
      skip: paginateOptions.offset,
      take: paginateOptions.limit,
    });

    return {
      entities,
      count,
    };
  }
}
