import { NextFunction, Request, Response } from 'express';
import { DatabaseSessionManager } from './database-session.manager';

export const databaseTransactionMiddleware = (req: Request, res: Response, next: NextFunction): void => {
  DatabaseSessionManager.initDatabaseSession();
  next();
};
