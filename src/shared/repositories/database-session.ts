import { Connection, EntityManager, EntityTarget, QueryRunner, Repository } from 'typeorm';
import { DatabaseSessionException } from './database-session.exception';

export class DatabaseSession {
  private isTransactionBegan!: boolean;
  private readonly queryRunner: QueryRunner;

  constructor(private readonly dbConnection: Connection) {
    this.queryRunner = this.dbConnection.createQueryRunner();
  }

  async transactionStart(): Promise<void> {
    if (this.isTransactionBegan) {
      throw DatabaseSessionException.createTransactionAlreadyStartedException();
    }
    this.isTransactionBegan = true;
    await this.queryRunner.connect();
    await this.queryRunner.startTransaction();
  }

  async transactionCommit(): Promise<void> {
    await this.queryRunner.commitTransaction();
    await this.queryRunner.release();
    this.isTransactionBegan = false;
  }

  async transactionRollback(): Promise<void> {
    await this.queryRunner.rollbackTransaction();
    await this.queryRunner.release();
    this.isTransactionBegan = false;
  }

  getQueryRunner(): QueryRunner {
    return this.queryRunner;
  }

  getRepository<TEntity>(entity: EntityTarget<TEntity>): Repository<TEntity> {
    return this.getEntityManager().getRepository(entity);
  }

  private getEntityManager(): EntityManager {
    return this.isTransactionBegan ? this.queryRunner.manager : this.dbConnection.createEntityManager();
  }
}
