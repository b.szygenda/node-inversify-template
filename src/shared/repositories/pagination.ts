export class PaginationOptions {
  readonly offset: number;
  readonly limit: number;
  constructor({ limit, page }: { page: number; limit: number }) {
    this.limit = limit;
    this.offset = page <= 0 ? 0 : page * limit - limit;
  }
}

export class PaginationResult<TEntity> {
  readonly entities!: TEntity[];
  readonly count!: number;
}
