import { DatabaseSessionManager } from './database-session.manager';
import { assignMetadata, copyMetadata } from '../metadata-utils';

export function Transaction(connectionName = 'default') {
  return (target: any, propertyKey: string, propertyDescriptor: PropertyDescriptor) => {
    const originalMethod = propertyDescriptor.value;
    const copiedMetadata = copyMetadata(originalMethod);

    // eslint-disable-next-line no-param-reassign,func-names
    propertyDescriptor.value = async function (...args: any) {
      const databaseSession = DatabaseSessionManager.getDatabaseSession(connectionName);

      try {
        await databaseSession.transactionStart();
        const result = await originalMethod.apply(this, args);

        await databaseSession.transactionCommit();

        return result;
      } catch (e) {
        await databaseSession.transactionRollback();

        throw e;
      }
    };

    assignMetadata(propertyDescriptor.value, copiedMetadata);
  };
}
