export class DatabaseSessionException extends Error {
  static createConnectionNotEstablishedException(): DatabaseSessionException {
    return new DatabaseSessionException('You should have at least one database connection');
  }

  static createTransactionAlreadyStartedException(): DatabaseSessionException {
    return new DatabaseSessionException(
      'Transaction already started for the given connection, commit current transaction before starting a new one.'
    );
  }

  static createCannotFindDatabaseSession(connectionName: string): DatabaseSessionException {
    return new DatabaseSessionException(`Cannot find DatabaseSession for connection: ${connectionName}`);
  }

  private constructor(message: string) {
    super(message);
  }
}
