export enum SortingType {
  ASC = 'ASC',
  DESC = 'DESC',
}

export type SortingOptions<TObjectToFilter> = {
  [P in keyof TObjectToFilter]?: SortingType;
};
