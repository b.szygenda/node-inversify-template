export enum Errors {
  validationFailed,
  entityNotFound,
  serverError,
}

const validationFailed = 'Validation failed.';
const entityNotFound = 'Entity was not found.';
const serverError = 'Internal server error.';

export const errorMsgsMap = new Map<number, string>([
  [Errors.validationFailed, validationFailed],
  [Errors.entityNotFound, entityNotFound],
  [Errors.serverError, serverError],
]);
