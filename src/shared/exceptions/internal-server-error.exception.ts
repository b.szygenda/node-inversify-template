import { StatusCodes } from 'http-status-codes';
import { HttpException, IExceptionParameters } from './http.exception';

export class InternalServerErrorException extends HttpException {
  constructor(exceptionParameters: IExceptionParameters) {
    super(HttpException.createBody(exceptionParameters, StatusCodes.INTERNAL_SERVER_ERROR), StatusCodes.INTERNAL_SERVER_ERROR);
  }
}
