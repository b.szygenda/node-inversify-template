import createHttpError from 'http-errors';
import { StatusCodes } from 'http-status-codes';

export type CustomErrorHandler = { messageHandler: (err: any | Error) => Record<any, any>; status: StatusCodes };

export type ErrorHandlers = Map<Function, CustomErrorHandler>;

export const EntityTooLargeExceptionHandler: CustomErrorHandler = {
  messageHandler: (_err: any) => ({ message: 'Payload too large' }),
  status: StatusCodes.REQUEST_TOO_LONG,
};

export function getErrorHandlers({ customHandlers, err }: { err: Error; customHandlers: ErrorHandlers }): Map<boolean, CustomErrorHandler> {
  const handlers: Map<boolean, CustomErrorHandler> = new Map([[err instanceof createHttpError.PayloadTooLarge, EntityTooLargeExceptionHandler]]);

  // eslint-disable-next-line no-restricted-syntax
  for (const [key, value] of customHandlers.entries()) {
    handlers.set(err instanceof key, value);
  }

  return handlers;
}
