import { StatusCodes } from 'http-status-codes';
import { ExceptionConstructor, HttpException } from './http.exception';

export class UnauthorizedException extends HttpException {
  constructor(exceptionDetails: Omit<ExceptionConstructor, 'status'>) {
    super(HttpException.createBody(exceptionDetails, StatusCodes.UNAUTHORIZED), StatusCodes.UNAUTHORIZED, exceptionDetails.previousException);
  }
}
