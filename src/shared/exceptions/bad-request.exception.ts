import { StatusCodes } from 'http-status-codes';
import { HttpException, IExceptionParameters } from './http.exception';

export class BadRequestException extends HttpException {
  constructor(exceptionDetails: IExceptionParameters) {
    super(HttpException.createBody(exceptionDetails, StatusCodes.BAD_REQUEST), StatusCodes.BAD_REQUEST);
  }
}
