import { StatusCodes } from 'http-status-codes';
import { HttpException, IExceptionParameters } from './http.exception';

export class ForbiddenException extends HttpException {
  constructor(exceptionParameters: IExceptionParameters) {
    super(HttpException.createBody(exceptionParameters, StatusCodes.FORBIDDEN), StatusCodes.FORBIDDEN);
  }
}
