import { Errors } from '../errors';

export class ValidationException {
  readonly value!: string;
  readonly msg!: string;
  readonly param!: string;
  readonly location!: string;
  readonly errorCode!: Errors;
}
