import { StatusCodes } from 'http-status-codes';
import { Errors, errorMsgsMap } from '../errors';

export const isUndefined = (obj: any): obj is undefined => typeof obj === 'undefined';
export const isNil = (obj: any): obj is null | undefined => isUndefined(obj) || obj === null;
export const isObject = (fn: any): fn is object => !isNil(fn) && typeof fn === 'object';
export const isString = (fn: any): fn is string => typeof fn === 'string';

export interface IExceptionParameters {
  errorCode: Errors;
  message?: string;
  details?: Array<{
    errorCode: Errors;
    message?: string;
    field?: string;
  }>;
}

export interface ExceptionConstructor extends IExceptionParameters {
  readonly previousException?: Error | any;
  readonly status: StatusCodes;
}

export class HttpException extends Error {
  constructor(
    private readonly response: string | Record<string, any>,
    private readonly status: number,
    private readonly previousException?: Error | any
  ) {
    super();
    this.initMessage();
    this.initName();
  }

  public initMessage() {
    if (isString(this.response)) {
      this.message = this.response;
    } else if (isObject(this.response) && isString((this.response as Record<string, any>).message)) {
      this.message = (this.response as Record<string, any>).message;
    } else if (this.constructor) {
      this.message = this.constructor.name.match(/[A-Z][a-z]+|[0-9]+/g)!.join(' ');
    }
  }

  public initName(): void {
    this.name = this.constructor.name;
  }

  public getResponse(): string | object {
    return this.response;
  }

  public getStatus(): number {
    return this.status;
  }

  public static createBody(exceptionDetails: IExceptionParameters, statusCode: number) {
    return {
      statusCode,
      errorCode: exceptionDetails.errorCode,
      message: exceptionDetails.message ?? errorMsgsMap.get(exceptionDetails.errorCode),
      details: exceptionDetails.details?.map((error) => ({
        errorCode: error.errorCode,
        message: error.message ?? errorMsgsMap.get(error.errorCode),
        field: error.field,
      })),
    };
  }
}
