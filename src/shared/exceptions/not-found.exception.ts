import { StatusCodes } from 'http-status-codes';
import { HttpException, IExceptionParameters } from './http.exception';

export class NotFoundException extends HttpException {
  constructor(exceptionParameters: IExceptionParameters) {
    super(HttpException.createBody(exceptionParameters, StatusCodes.NOT_FOUND), StatusCodes.NOT_FOUND);
  }
}
