import { StatusCodes } from 'http-status-codes';
import { HttpException, IExceptionParameters } from './http.exception';

export class UnprocessableEntityException extends HttpException {
  constructor(exceptionDetails: IExceptionParameters) {
    super(HttpException.createBody(exceptionDetails, StatusCodes.UNPROCESSABLE_ENTITY), StatusCodes.UNPROCESSABLE_ENTITY);
  }
}
