import { Router } from 'express';
import SampleRouter from './sample/routes';

const router = Router();

router.use('/', SampleRouter);

export default router;
