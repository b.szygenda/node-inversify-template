import { initApp, clearSchema, composeWithBaseUrl } from '../../../../test-utils/init-test-app';
import { Application } from 'express';
import request from 'supertest';
import { StatusCodes } from 'http-status-codes';

describe('Sample integration tests', () => {
  let app: Application;

  beforeAll(async () => {
    app = await initApp();
  });

  beforeEach(async () => {
    jest.resetAllMocks();
    await clearSchema();
  });

  describe('GET /', () => {
    const getRoute = () => composeWithBaseUrl('/');

    it('Should return 200', async () => {
      const response = await request(app).get(getRoute());

      expect(response.status).toBe(StatusCodes.OK);
    });
  });
});
