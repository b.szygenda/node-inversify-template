import { Response, Request } from 'express';
import { Container } from '../../../ioc/container';
import { SampleService } from '../../../domain/services/sample.service';
import { StatusCodes } from 'http-status-codes';
import { SampleCommand } from '../../../application/commands/sample/sample.command-handler';
import { SampleQuery } from '../../../application/queries/sample/sample.query-handler';
import { QueryBus } from '../../../shared/events/query-bus';
import { CommandBus } from '../../../shared/events/command-bus';

export class SampleController {
  static async get(req: Request, res: Response) {
    const container = Container.getInstance();
    const service = container.get(SampleService);

    service.sampleMethod();

    res.status(StatusCodes.OK).json({ message: 'Success!' });
  }

  static async postWithCommand(req: Request, res: Response) {
    const container = Container.getInstance();
    const commandBus = container.get(CommandBus);

    const command = new SampleCommand({});

    await commandBus.handle(command);

    res.status(StatusCodes.CREATED).json({ message: 'Success!' });
  }

  static async getWithQuery(req: Request, res: Response) {
    const container = Container.getInstance();
    const queryBus = container.get(QueryBus);

    const query = new SampleQuery({});

    await queryBus.handle(query);

    res.status(StatusCodes.OK).json({ message: 'Success!' });
  }
}
