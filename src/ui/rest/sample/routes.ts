import { Router } from 'express';
import { SampleValidator } from './sample.validator';
import { SampleController } from './sample.controller';

const router = Router();

router.get('/', SampleValidator.get, SampleController.get);
router.post('/', SampleController.postWithCommand);
router.get('/query', SampleController.getWithQuery);

export default router;
