import { validateMiddleware } from '../../../shared/middleware/validate.middleware';

export class SampleValidator {
  static get = [validateMiddleware];

  private constructor() {}
}
