import 'reflect-metadata';
import './pre-start'; // Must be the first import
import 'newrelic';
import { createConnection } from 'typeorm';
import app from './server';
import config from './ormconfig';
import { Container } from './ioc/container';
import { Config } from './utils/config/config';
import { Logger } from './shared/services/logger/logger';

const container = Container.getInstance();
const appConfig = container.get(Config);
const logger = container.get(Logger);

appConfig
  .validate()
  .then(() => createConnection(config))
  .then(async () => {
    const port = Number(process.env.PORT /* || 1234 TODO ENTER DEFAULT PORT NUMBER */);

    app.listen(port, () => {
      logger.log(`Express server started on port: ${port}`);
    });

    await Promise.all([
      /* Enter all post start jobs here or remove this */
    ]);
  });
