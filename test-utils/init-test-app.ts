import { config } from 'dotenv';
import { Application } from 'express';
import path, { join } from 'path';
import { createConnection, getRepository } from 'typeorm';
import app from '../src/server';
import { SampleEntity } from '../src/domain/entities/sample.entity';
import { Config } from '../src/utils/config/config';
import { Container } from '../src/ioc/container';
import { EntitySchemas } from '../src/domain/entities/entity-schemas';

async function initDatabase(): Promise<void> {
  await createConnection({
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: Number(process.env.POSTGRES_PORT),
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    migrationsRun: true,
    synchronize: false,
    entities: EntitySchemas,
    migrations: [`${__dirname}/../**/migrations/*.ts`],
    cli: {
      migrationsDir: 'src/migrations',
    },
  });
}

export async function clearSchema() {
  await getRepository(SampleEntity).createQueryBuilder().delete().execute();
}

export function mockServices() {
  // TODO:
  // const container = Container.getInstance();
  // Rebind services here
}

function mockConfig(): void {
  config({ path: join(__dirname, 'test-int.env') });
  const container = Container.getInstance();

  container
    .rebind(Config)
    .toDynamicValue(() => Config.load())
    .inSingletonScope();
}

export async function initApp(): Promise<Application> {
  mockConfig();
  await initDatabase();
  mockServices();

  return app;
}

// TODO: Replace SERVICE with the name of the service
export function composeWithBaseUrl(endpoint: string): string {
  return path.join(`/SERVICE/api/v2`, endpoint);
}
