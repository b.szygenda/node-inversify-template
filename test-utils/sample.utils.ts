import { SampleEntity } from '../src/domain/entities/sample.entity';
import * as faker from 'faker';
import { getRepository } from 'typeorm';

export const createSample = (opts?: Partial<SampleEntity>): SampleEntity =>
  new SampleEntity({
    id: faker.datatype.uuid(),
    ...opts,
  });

export const saveSample = async (opts?: Partial<SampleEntity>): Promise<SampleEntity> => getRepository(SampleEntity).save(createSample(opts));
