#!make
include .env

$(eval export $(shell sed -ne 's/ *#.*$//; /./ s/=.*$$// p' .env))

generate-migration:
	npm run typeorm:migrate $(MIGRATION_NAME)

init-test-db:
	./test-utils/init-test-db.sh
