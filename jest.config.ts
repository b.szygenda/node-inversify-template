export default {
  moduleDirectories: ['node_modules', '<rootDir>'],
  moduleFileExtensions: ['js', 'json', 'ts'],
  roots: ['src'],
  testRegex: '.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  coverageDirectory: 'coverage',
  preset: 'ts-jest',
  testEnvironment: 'node',
  modulePaths: ['node_modules', '<rootDir>/src'],
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/src/$1',
  },
  collectCoverageFrom: [
    '!<rootDir>/src/migrations/*',
    '!<rootDir>/src/server.ts',
    '!<rootDir>/src/v2/domain/events/*',
    '!<rootDir>/test-utils/*',
    '!<rootDir>/src/index.ts',
    '!<rootDir>/src/ormconfig.ts',
    '!<rootDir>/src/pre-start/*',
    '!<rootDir>/src/**/*dto.ts',
    '!<rootDir>/src/**/*exception.ts',
  ],
  setupFilesAfterEnv: ['./jest.setup.ts'],
};
